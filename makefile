
all: markdown ts-fetch fastapi-server

ts-fetch:
	npx @openapitools/openapi-generator-cli generate -i api-gestocks.yml -g typescript-fetch -o /build/ts-fetch/

python:
	npx @openapitools/openapi-generator-cli generate -i api-gestocks.yml -g python -o /build/python/

markdown:
	npx @openapitools/openapi-generator-cli generate -i api-gestocks.yml -g markdown -o /build/markdown/

fastapi-server:
	npx @openapitools/openapi-generator-cli generate -i api-gestocks.yml -g python-fastapi -o /build/fastapi/

clean :
	rm -rf build
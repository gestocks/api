# API
## C'est quoi ?
Il s'agit de la description de l'API HTTP permettant au client et au serveur de communiquer.
La norme utilisée est openAPI

## Generation :
L'utilitaire de génération de code est installable avec *npm*, grace à la commande `sudo npm install @openapitools/openapi-generator-cli -g`


Pur générer le boilerplate du serveur, on utilise:

```bash
openapi-generator-cli generate -g python-flask -i api-gestocks.yml -o flask
```

Pour générer la librairie client en typescript, on utilise:

```bash 
openapi-generator-cli generate -g typescript-fetch -i api-gestocks.yml -o fetch-ts
```

Et pour générer la documentation :
```bash 
openapi-generator-cli generate -g markdown -i api-gestocks.yml -o markdown
```